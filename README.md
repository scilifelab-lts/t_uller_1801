# N.B.
All source codes mentioned here with rackham path location can also be found in this repository in folder `src/`.  
Note that all relative path to files in the source codes will currently need to be updated to reproduce results.  
Hyperlink text points to reference webpage or project wiki page.  
This repository is a work in progress.  


# WGBS read mapping and extraction of methylation values

The processing of 64 fastq files and quantification of cytosine methylation were carried out within the framework of nf-core [methylseq](https://github.com/nf-core/methylseq) workflow version 1.5.  
The pipeline is built using Nextflow and processes data using the following steps:

 - FastQC - read quality control  
 - TrimGalore - adapter trimming  
 - Alignment - aligning reads to reference genome  
 - Deduplication - deduplicating reads  
 - Methylation Extraction - calling cytosine methylation steps  
 - Bismark Reports - single-sample and summary analysis reports  
 - Qualimap - tool for genome alignments QC  
 - Preseq - tool for estimating sample complexity  
 - MultiQC - aggregate report, describing results of the whole pipeline  
 - Pipeline Info - reports from nextflow about the pipeline run  


The nextflow command to execute the workflow in rackham is  

```bash
nextflow run nf-core/methylseq -r 1.5 -profile uppmax --reads '/proj/snic2020-16-42/uppstore2018201/raw/*_R{1,2}_*.fastq.gz' 
--fasta /proj/snic2020-16-42/louella/Bismark.04242020.FINAL/REFERENCE/ncbi-genomes-2020-04-11/GCF_003990815.1_ASM399081v1_genomic.fasta --save_reference 
--unmapped --save_trimmed --outdir /proj/snic2020-16-42/louella/Bismark.04242020.FINAL --project snic2020-15-61
```
Reference genome used is `/proj/snic2020-16-42/louella/Bismark.04242020.FINAL/REFERENCE/ncbi-genomes-2020-04-11/GCF_003990815.1_ASM399081v1_genomic.fasta`.  

Output results directory is `/proj/snic2020-16-42/louella/Bismark.04242020.FINAL`, wherein  
the Bismark-aligned deduplicated BAMS are in `/proj/snic2020-16-42/louella/Bismark.04242020.FINAL/bismark_deduplicated`;  
the coverage files used for DMC are in `/proj/snic2020-16-42/louella/Bismark.04242020.FINAL/bismark_deduplicated/bismark_methylation_calls/methylation_coverage` with columns    
`<chromosome> <start position> <end position> <methylation percentage> <count methylated> <count unmethylated>`.


For more technical information about this nextflow run, please see html reports:  
`/proj/snic2020-16-42/louella/Bismark.04242020.FINAL/pipeline_info/results_description.html`  
`/proj/snic2020-16-42/louella/Bismark.04242020.FINAL/pipeline_info/execution_report.html.1`


# Sample QC

The nextflow methylseq run produced a summary [MultiQC report](https://wabi-wiki.scilifelab.se/display/TUL/new+Bismark+remap+04.2020) `/proj/snic2020-16-42/louella/Bismark.04242020.FINAL/MultiQC/multiqc_report.html`,  
which contains read mapping quality, coverage and many sequencing QC parameters.  
We used these reports to flag,drop poor,failed WGBS libraries.  

For global intersample variance comparison as reported in [wiki](https://wabi-wiki.scilifelab.se/display/TUL/remap+Preliminary+QC)
, we consider the matrix of %Methylation.  
This matrix was generated by `bedtools unionbedg`, as in script  
`/proj/snic2020-16-42/louella/Bismark.04242020.FINAL/union_bedGraph.sh`.  
Matrices of %Methylation are located in:  
for all 64 libraries, `/proj/snic2020-16-42/louella/Bismark.04242020.FINAL/bismark_methylation_calls/bedGraph/coverage_union.txt`;  
for all 58 libraries after dropping 6 fails due to poor mapping quality (< 15% mapping rate, < 1X mean coverage, < 5x median coverage), `/proj/snic2020-16-42/louella/Bismark.04242020.FINAL/bismark_methylation_calls/bedGraph/coverage_union_mapping15.txt`;  
for all 58 libraries after dropping sites with NA in at least one sample, `/proj/snic2020-16-42/louella/Bismark.04242020.FINAL/bismark_methylation_calls/bedGraph/coverage_union_mapping15.NAremoved.txt`.  
The [QC reports](https://wabi-wiki.scilifelab.se/display/TUL/remap+Preliminary+QC) in the wiki were generated by R Markdown scripts  
`/proj/snic2020-16-42/louella/src/PrelimQC_remap.06252020_outliers.removed.Rmd`,  
`/proj/snic2020-16-42/louella/src/PrelimQC_remap.06252020.Rmd`.  




# Differential methylation analysis
Bioconductor R package [methylKit_1.12.0](https://bioconductor.org/packages/release/bioc/vignettes/methylKit/inst/doc/methylKit.html) was used to carry out differential methylation analysis to find DMCs.

The final filtering strategy of CpG sites is

 - consider CpG sites with minimum of 5 total read counts in all samples in all F-generations
 - sites were filtered to consider only variable sites with sample standard deviation of  ≥ 0.5 in percent methylation values
 - same set of all control samples across generations was used for each statistical test, i.e., 3 cases F1 vs 9 controls F1+F2+F4  
 
Full report is in [wiki](https://wabi-wiki.scilifelab.se/display/TUL/DMCs_RC5all_vs.9controls). 


The main R wrapper script that executes the different functionality of `methyKit` is  
`/proj/snic2020-16-42/louella/src/DMC.R`  
```R
Usage: DMC.R [-[-help|h]] [-[-metadata|m] <character>] [-[-cores|t] <integer>] [-[-sd|s] <double>] [-[-coverage|c] <integer>] [-[-qval|q] <double>]
    -h|--help        print the usage of the command
    -m|--metadata    metadata (REQUIRED) containing minimum of 3 columns: file, sampleid, group
    -t|--cores       number of cores to use: default [ 1 ] 
    -s|--sd          minimum number of standard deviation to filter sites: default [ 0.5 ]
    -c|--coverage    minimum number of read counts at each sites: default [ 10 ]
    -q|--qval        FDR cutoff: default [ 0.05 ]
```  
The bash script that runs `DMC.R` for the different case-control pairs and for each Fgen is here  
`/proj/snic2020-16-42/louella/src/run.DMC_RC5all_9controls.sh`,  
and all results as organised by each test in different subfolder are here  
`/proj/snic2020-16-42/louella/results.DMCs_RC5all_vs.9controls`.  
Subfolders can be found by  
`ls -d /proj/snic2020-16-42/louella/results.DMCs_RC5all_vs.9controls/*/`. 


Note that `DMC.R` was executed independently for each 3-vs-9 case-control (eg result subfolder `T1-Z1_vs_T1-C1_vs_T1-C2_vs_T1-C4/`)  
and also for 9-vs-9 case-control (eg result subfolder `T1-C0_vs_T1-Z0/`).  
However, as outlined in the filtering strategy, the final set of tested CpG sites is taken from the test of 9-vs-9 case-control.  
Hence, the table of DMCs in each test of 3-vs-9 case-control was further subsetted and pvalues readjusted accordingly for multiple testing with reduced number of sites.  
Overall, the final subsetted table of DMCs in each Fgen for a given case-control test uses the same set of CpG sites.  


The script used to create the final subsetted table of DMCs is  
`/proj/snic2020-16-42/louella/src/run.create.final.DE.tables.sh`,  
`/proj/snic2020-16-42/louella/src/create.final.DE.tables.R`.    
The final subsetted table of DMCs can be found by  
`ls /proj/snic2020-16-42/louella/results.DMCs_RC5all_vs.9controls/*/final*`.  


Furthermore,  the bash script that runs `DMC.R` for the permutation test is  
`/proj/snic2020-16-42/louella/src/run.DMC_RC5all_9control_permute.labels.sh`,  
and all results as organised by each test in different subfolder are here  
`/proj/snic2020-16-42/louella/results.DMCs_RC5all_vs.9controls_permute`.




# Finding stable transgenerational DMCs
Full reports are in [wiki](https://wabi-wiki.scilifelab.se/display/TUL/DMCs_RC5all_vs.9controls), and generated by R Markdown scripts  
`src/summarise.DMCs.vs9controls.Rmd`,  
`src/summarise.DMCs.vs9controls_LR.Rmd`.


# Annotation of DMCs
Script is here, NB still at work in progress  
`src/summarise.DMCs.vs9controls.R`.
