#!/bin/bash -l


#fullmeta=$1
fullmeta="/proj/snic2020-16-42/louella/Bismark.04242020.FINAL/metadata.uppmax.FINAL_10302020.tsv"


DMC="/proj/snic2020-16-42/louella/src/DMC.R"



##########################################################################################################################################
# select L and C only ,  then permute gen  & L/C

batch="T2"
control="C"

treats=(A L T)
gens=(1 2 4)

for treat in ${treats[@]}
do

#######
# permuting labels of input metadata
pmeta="${batch}-permute${treat}.metadata.tsv"
header=$(head -n1 $fullmeta)

key=$(echo -e "/${batch}-${treat}")
cat $fullmeta | awk -F"\t" -v ID="$key" '$1~ID'  > $pmeta
key=$(echo -e "/${batch}-${control}")
cat $fullmeta | awk -F"\t" -v ID="$key" '$1~ID'  >> $pmeta



cut -f1 $pmeta > tmp.permute
cut -f2- $pmeta | shuf | paste tmp.permute - > tmp
mv tmp $pmeta
sed  -i "1i ${header}" $pmeta
#######



	for gen in ${gens[@]}
	do
	#echo "$batch $treat  $gen"
	echo -e "/${batch}-${treat}${gen}-"

	ometa="permute.${batch}-${treat}${gen}.metadata.tsv"

	head -n1 $pmeta > $ometa
	key=$(echo -e "${batch}-${treat}${gen}-")
	cat $pmeta | awk -F"\t" -v ID="$key" '$2~ID'  >> $ometa
	key=$(echo -e "${batch}-${control}")
        cat $pmeta | awk -F"\t" -v ID="$key" '$2~ID'  >> $ometa

	#sbatch -A snic2020-15-61 -p core -n 2 -t 120:00:00 -J DMC -e DMC_%j_error.txt -o DMC_%j_out.txt Rscript $DMC -m $ometa -t 2 -q 0.2


	osh="${batch}-${treat}${gen}.metadata.sh"
	ojob="${batch}-${treat}${gen}"
	
        cat > $osh << EOF
#!/bin/bash -l

#SBATCH -A snic2020-15-61
#SBATCH -p core
#SBATCH -n 8
#SBATCH -t 48:00:00
#SBATCH -J $ojob
#SBATCH -e ${ojob}_%j_error.txt
#SBATCH -o ${ojob}_%j_out.txt

module load bioinfo-tools

Rscript $DMC -m $ometa -t 8 -q 0.05 -c 5 -s 0

exit 0;
EOF






	sbatch $osh
	echo
	done

done
##########################################################################################################################################
# select all treatments and C only ; FIX all 9 C  then permute gen & random treats


batch="T2"
control="C"

treat=(M)
gens=(1 2 4)



#######
# permuting labels of input metadata
pmeta="${batch}-permuteM.metadata.tsv"
header=$(head -n1 $fullmeta)


#cat $fullmeta | grep "/T2-" | grep -v "/T2-C" | shuf -n 9  | cut -f1 | paste - permute.treat.columns.txt 
cat $fullmeta | grep "/T2-" | grep -v "/T2-C" | shuf -n 9  | cut -f1 | paste - permute.treat.columns.txt > $pmeta
key=$(echo -e "/${batch}-${control}")
cat $fullmeta | awk -F"\t" -v ID="$key" '$1~ID'  >> $pmeta
sed  -i "1i ${header}" $pmeta
#cat $pmeta
#######



        for gen in ${gens[@]}
        do
        #echo "$batch $treat  $gen"
        echo -e "/${batch}-${treat}${gen}-"

        ometa="permute.${batch}-${treat}${gen}.metadata.tsv"

        head -n1 $pmeta > $ometa
        key=$(echo -e "${batch}-${treat}${gen}-")
        cat $pmeta | awk -F"\t" -v ID="$key" '$2~ID'  >> $ometa
        key=$(echo -e "${batch}-${control}")
        cat $pmeta | awk -F"\t" -v ID="$key" '$2~ID'  >> $ometa

        #sbatch -A snic2020-15-61 -p core -n 2 -t 120:00:00 -J DMC -e DMC_%j_error.txt -o DMC_%j_out.txt Rscript $DMC -m $ometa -t 2 -q 0.2


        osh="${batch}-${treat}${gen}.metadata.sh"
        ojob="${batch}-${treat}${gen}"

        cat > $osh << EOF
#!/bin/bash -l

#SBATCH -A snic2020-15-61
#SBATCH -p core
#SBATCH -n 8
#SBATCH -t 48:00:00
#SBATCH -J $ojob
#SBATCH -e ${ojob}_%j_error.txt
#SBATCH -o ${ojob}_%j_out.txt

module load bioinfo-tools

Rscript $DMC -m $ometa -t 8 -q 0.05 -c 5 -s 0

exit 0;
EOF

      sbatch $osh
        echo
	done




##########################################################################################################################################

batch="T1"
control="C"
treats=(Z)
gens=(1 2 4)


#######
# permuting labels of input metadata
pmeta="${batch}-permute.metadata.tsv"
header=$(head -n1 $fullmeta)
key=$(echo -e "/${batch}-")
cat $fullmeta | awk -F"\t" -v ID="$key" '$1~ID'  > $pmeta


cut -f1 $pmeta > tmp.permute
cut -f2- $pmeta | shuf | paste tmp.permute - > tmp
mv tmp $pmeta
sed  -i "1i ${header}" $pmeta
#######



for treat in ${treats[@]}
do
        for gen in ${gens[@]}
        do
        #echo "$batch $treat  $gen"
        echo -e "/${batch}-${treat}${gen}-"

        ometa="permute.${batch}-${treat}${gen}.metadata.tsv"

        head -n1 $pmeta > $ometa
        key=$(echo -e "${batch}-${treat}${gen}-")
        cat $pmeta | awk -F"\t" -v ID="$key" '$2~ID'  >> $ometa
        key=$(echo -e "${batch}-${control}")
        cat $pmeta | awk -F"\t" -v ID="$key" '$2~ID'  >> $ometa


        osh="${batch}-${treat}${gen}.metadata.sh"
        ojob="${batch}-${treat}${gen}"

        cat > $osh << EOF
#!/bin/bash -l

#SBATCH -A snic2020-15-61
#SBATCH -p core
#SBATCH -n 8
#SBATCH -t 48:00:00
#SBATCH -J $ojob
#SBATCH -e ${ojob}_%j_error.txt
#SBATCH -o ${ojob}_%j_out.txt

module load bioinfo-tools

Rscript $DMC -m $ometa -t 8 -q 0.05 -c 5 -s 0

exit 0;
EOF


        sbatch $osh



        echo
        done
done


##########################################################################################################################################


## All generations of T1-Z

head -n1 permute.T1-Z1.metadata.tsv > permute.T1-Z0.metadata.tsv
cat permute.T1-Z*.metadata.tsv | grep -v file | sort | uniq | sort -V -k3,3 -k2,2 |  awk -F"\t" '{gsub(/T1-C/,"T1-C0",$2);gsub(/T1-Z/,"T1-Z0",$2);print}' OFS="\t" >> permute.T1-Z0.metadata.tsv


osh="T1-Z0.metadata.sh"
ojob="T1-Z0"

cat > $osh << EOF
#!/bin/bash -l

#SBATCH -A snic2020-15-61
#SBATCH -p core
#SBATCH -n 8
#SBATCH -t 48:00:00
#SBATCH -J $ojob
#SBATCH -e ${ojob}_%j_error.txt
#SBATCH -o ${ojob}_%j_out.txt

module load bioinfo-tools

Rscript $DMC -m permute.T1-Z0.metadata.tsv -t 8 -q 0.05 -c 5

exit 0;
EOF

#sbatch $osh


## All generations of T2-L

head -n1 permute.T2-L1.metadata.tsv > permute.T2-L0.metadata.tsv
cat permute.T2-L*.metadata.tsv | grep -v file | sort | uniq | sort -V -k3,3 -k2,2 |  awk -F"\t" '{gsub(/T2-C/,"T2-C0",$2);gsub(/T2-L/,"T2-L0",$2);print}' OFS="\t" >> permute.T2-L0.metadata.tsv


osh="T2-L0.metadata.sh"
ojob="T2-L0"

cat > $osh << EOF
#!/bin/bash -l

#SBATCH -A snic2020-15-61
#SBATCH -p core
#SBATCH -n 8
#SBATCH -t 48:00:00
#SBATCH -J $ojob
#SBATCH -e ${ojob}_%j_error.txt
#SBATCH -o ${ojob}_%j_out.txt

module load bioinfo-tools

Rscript $DMC -m permute.T2-L0.metadata.tsv -t 8 -q 0.05 -c 5

exit 0;
EOF

#sbatch $osh




## All generations of T2-M

head -n1 permute.T2-M1.metadata.tsv > permute.T2-M0.metadata.tsv
cat permute.T2-M*.metadata.tsv | grep -v file | sort | uniq | sort -V -k3,3 -k2,2 |  awk -F"\t" '{gsub(/T2-C/,"T2-C0",$2);gsub(/T2-M/,"T2-M0",$2);print}' OFS="\t" >> permute.T2-M0.metadata.tsv


osh="T2-M0.metadata.sh"
ojob="T2-M0"

cat > $osh << EOF
#!/bin/bash -l

#SBATCH -A snic2020-15-61
#SBATCH -p core
#SBATCH -n 8
#SBATCH -t 48:00:00
#SBATCH -J $ojob
#SBATCH -e ${ojob}_%j_error.txt
#SBATCH -o ${ojob}_%j_out.txt

module load bioinfo-tools

Rscript $DMC -m permute.T2-M0.metadata.tsv -t 8 -q 0.05 -c 5

exit 0;
EOF

#sbatch $osh

