#!/bin/bash -l



for dir in $(ls -d T*-C0_*0)
do


	echo "dir $dir"



	osh="script.${dir}.finalDEtab.sh"
	ojob="${dir}.finalDEtab"

	echo $osh	

        cat > $osh << EOF
#!/bin/bash -l

#SBATCH -A snic2020-15-61
#SBATCH -p core
#SBATCH -n 4
#SBATCH -t 12:00:00
#SBATCH -J $ojob
#SBATCH -e ${ojob}_%j_error.txt
#SBATCH -o ${ojob}_%j_out.txt

module load bioinfo-tools

Rscript ../src/create.final.DE.tables.R $dir


exit 0;
EOF






	sbatch $osh



done
