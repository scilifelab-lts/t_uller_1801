
<!--
In laptop
Run this code by:
Rscript -e 'Sys.setenv(RSTUDIO_PANDOC="/usr/lib/rstudio/bin/pandoc");rmarkdown::render("PrelimQC_remap.06252020.Rmd")'



In UPPMAX
Run this code by:

module load R/3.6.0
module load pandoc
Rscript -e 'rmarkdown::render("PrelimQC_remap.06252020.Rmd")'

R libraries are here:
~/R/x86_64-redhat-linux-gnu-library/3.6/

-->

---
title: "preliminary QC - remap"
date: "`r format(Sys.time(), '%d %B, %Y')`"
output: 
  html_document:
    number_sections: true
---
<br>
<br>


# Aim
* to revisit the global methylation profile of Daphnia (invertebrate)
* to observe the effect of different stimuli in the global methylome
* Failed samples are removed here.  Sample outliers are still considered here.

  <!--
  + % CpG
  + % methylated
  + Cpg density in the genome
  -->
  
* to revisit Data Quality so as to inform future revision of QC cutoffs eg read count, filtering of sites
* to have the first ascertainment of inter vs intra group variation 

<br>
<br>


# Summary

* Round 1 of sample filtering: removed **8** samples/bams with unique mapping rate of < 15%.  Number of samples left is **58**.
  + These 8 samples removed are also characterised by >2% mCpG as opposed to ~1.3% mCpG from high mapping rate samples.
  
* Found **6** outlier samples/bams based from PCA, unsupervised clustering.
  + T1-Z1-B2 has mapping rate of 21.2% and has 2.9% mCpG
  + T2-C1-B3 has mapping rate of 21.7%
  + T2-T2-B1 has mapping rate of 23%
  + Three replicates consisting T2-A1-B1 (mapping rate 19.2%, 3.5% mCpG), T2-A1-B2, T2-A1-B4
  
* Overall, samples/bams that are either failed libraries or outliers have low unique mapping rate < 25%, higher > 2% mCpG, low median coverage < 10x.
  
* When considering high M (sample median >= 80%), treatment **A = 5-azacytidine** is a clear outlier group.  Note that 5-azac inhibits DNMT causing global hypomethylation.
  + 5-azac treated samples have overall lower hypomethylation genome-wide profile.


 
* There is a strong batch effect due to experiment batch T1, T2.  Batch-specific control (available!) must be used to compare to a treatment group. 

* Without per-sample normalisation of M values, the dominant source of variation is driven by a handful of outlier samples.
  + Z-score normalisation (mean centered and scaled to 1 standard deviation) is better in discriminating between groups than quantile normalisation.
  + Improved group clustering is observed when low and high M are analysed separately --> need to dig deeper into the data!



  
<br>
<br>



```{r,include=FALSE}

rm(list=ls()) 
library("data.table")
library("affy")
library("RColorBrewer")
library("genefilter")
library("ggplot2")
library("hexbin")
library("KernSmooth")
library("dendextend")
library("preprocessCore")
library("rmarkdown")
library("knitr")
library("tidyr")



#Sys.setenv(RSTUDIO_PANDOC="/usr/lib/rstudio/bin/pandoc")
#setwd("/home/louella/Project_ULLER/")

```



#
***
***
***
#
<br>
<br>



# Levels of feature filtering 
**to consider only relevant most variable sites**

###############################################################################################################################################

```{r,echo=FALSE}
#meth.mat<-"/home/louella/Project_ULLER/Bismark.04242020.FINAL/bismark_methylation_calls/bedGraph/coverage_union_mapping15.NAremoved_test10K.txt"
meth.mat<-"/proj/snic2020-16-42/louella/Bismark.04242020.FINAL/bismark_methylation_calls/bedGraph/coverage_union_mapping15.NAremoved.txt"

message("Original matrix is ", meth.mat)
#meth.mat<-"/proj/snic2020-16-42/uppstore2018201/extracted/coverage_union.txt"
meth.mat<-fread(meth.mat)
meth.mat=unite(meth.mat,FEAT,c("chrom","start","end"),sep="-")
featname=meth.mat$FEAT
featname.orig=featname
write.table(data.frame(featname),file="/proj/snic2020-16-42/louella/Bismark.04242020.FINAL/bismark_methylation_calls/bedGraph/sites_coverage_union_mapping15.NAremoved.txt",quote=FALSE,row.names = FALSE,col.names = FALSE)


meth.mat<-meth.mat[,-c(1)]
rownames(meth.mat)=featname
message("Dimension of the matrix -- minus NA in at least one sample ")

#message("Dimension of the matrix -- ORIGINAL ")
dim(meth.mat)

meth.mat.sans.NA_allsd.allM<-meth.mat
meth.mat.sans.NA_allsd.highM<-meth.mat[rowMedians(as.matrix(meth.mat))>=80,]

# 10M sites X 54 samples : RAW matrix
#10070330       57
```
<br>

```{r,echo=FALSE}

sd.meth.mat<-rowSds(meth.mat)


meth.mat<-meth.mat[which(sd.meth.mat>=10),]
rownames(meth.mat)=featname[which(sd.meth.mat>=10)]
featname= rownames(meth.mat)
featname.sd10=featname

write.table(data.frame(featname),file="/proj/snic2020-16-42/louella/Bismark.04242020.FINAL/bismark_methylation_calls/bedGraph/sites_coverage_union_mapping15.NAremoved_sd10.txt",quote=FALSE,row.names = FALSE,col.names = FALSE)

message("Dimension of the matrix -- minus low varying sites at sd < 10 + minus NA in at least one sample")
dim(meth.mat)

meth.mat.sans.NA_highsd.allM<-meth.mat
meth.mat.sans.NA_highsd.highM<-meth.mat[rowMedians(as.matrix(meth.mat))>=80,]

featname=featname[which(rowMedians(as.matrix(meth.mat))<=20)]
featname.sd10.median20=featname
meth.mat<-meth.mat[rowMedians(as.matrix(meth.mat))<=20,]
message("Dimension of the matrix -- only hypo M at median M <= 20% + minus low varying sites at sd < 10 + minus NA in at least one sample")
dim(meth.mat)

write.table(data.frame(featname),file="/proj/snic2020-16-42/louella/Bismark.04242020.FINAL/bismark_methylation_calls/bedGraph/sites_coverage_union_mapping15.NAremoved_sd10_rowmedian20.txt",quote=FALSE,row.names = FALSE,col.names = FALSE)

message("Dimension of the matrix -- only hyper M at median M >= 80% + minus low varying sites at sd < 10 + minus NA in at least one sample")
dim(meth.mat.sans.NA_highsd.highM)


```
<br>
```{r,echo=FALSE}
#ONLY consider sites where present in ALL samples

meth.mat.sans.NA<-na.omit(meth.mat)  # <-----  Final matrix to use  !!!

#message("Dimension of the matrix -- minus NA in at least one sample ")
#dim(meth.mat.sans.NA)


#6310496      54

```
<br>


```{r,echo=FALSE}
#removing sites where all samples are 0%
meth.mat.sans.NA<-meth.mat.sans.NA[rowSums(meth.mat.sans.NA)>0,]
message("Dimension of the matrix -- minus 0% Methylation in ALL samples")
dim(meth.mat.sans.NA)
```
<br>


```{r,echo=FALSE}
#removing sites where all samples are 100%
meth.mat.sans.NA<-meth.mat.sans.NA[rowSums(meth.mat.sans.NA)<ncol(meth.mat.sans.NA)*100,]
message("Dimension of the matrix -- minus 100% Methylation in ALL samples")
dim(meth.mat.sans.NA)
#write.table(meth.mat.sans.NA,file="DATA/MATRIX/coverage_union_NA.rm.txt",sep=" ",quote=FALSE,row.names = FALSE)
#reduced<-meth.mat.sans.NA[rowMeans(as.matrix(meth.mat.sans.NA))<=2.5,]
#dim(reduced)
#`r knitr::knit_exit()`
```
#
***
***
***
#
<br>
<br>


###############################################################################################################################################
# Sample Groups

**T1 and T2 are Batches**
<br>
Treatments:\
**A = 5-azacytidine**,   **C = control**,   **L = microcystin**,   **T = temperature**,   **Z = zinc**\


```{r,include=FALSE}
## colours per group
textsplit<-function(x){substr(unlist(strsplit(x,split="B",fixed=TRUE))[1],1,5)}

grps<-sort(unique(unlist(lapply(colnames(meth.mat.sans.NA),textsplit))))
length(grps)

pcolors<-rainbow(length(grps))
ppcolors<-replicate(ncol(meth.mat.sans.NA),"black")

for (i in 1:length(grps)){
  
  grp<-grps[i]
  #message(" group ", grp )
  ppcolors[unlist(lapply(colnames(meth.mat.sans.NA),textsplit))== grp] <- pcolors[i]
  
}
```

There are `r length(grps)` groups:  6 treatments (N.B. 2 controls) X 3 F-generations\
`r sort(grps)`

Replicates per treatment-Fgeneration:\
```{r,echo=FALSE}
print(table(sort(unlist(lapply(colnames(meth.mat.sans.NA),function(x){substr(unlist(strsplit(x,split="B",fixed=TRUE))[1],1,5)})))))
```
<br>



###############################################################################################################################################


```{r,include=FALSE}

# coarse grained groups
textsplit<-function(x){substr(unlist(strsplit(x,split="-",fixed=TRUE))[2],1,1)}

grps<-sort(unique(unlist(lapply(colnames(meth.mat.sans.NA),textsplit))))
length(grps)

pcolors<-rainbow(length(grps))
ppcolors2<-replicate(ncol(meth.mat.sans.NA),"black")

for (i in 1:length(grps)){
  
  grp<-grps[i]
  #message(" group ", grp )
  ppcolors2[unlist(lapply(colnames(meth.mat.sans.NA),textsplit))== grp] <- pcolors[i]
  
}
```
There are `r length(grps)` groups:  \
`r sort(unique(grps))`
<br>

**Sample label nomenclature**: T2-A2-B3 means Batch _T2_, Treatment _5-azacitidine_ in _generation 2_, replicate _B3_

<br>
<br>

#
***
***
***
#
<br>
<br>


###############################################################################################################################################

# Genome wide profile is HYPO methylation 
By selecting only highly variable sites (ie high standard deviation sd), marked differences between treatment groups can now be observed\
and variation at higher M is now seen.\
Overall, controls have more sites with higher M (though still very low M) in comparison to other groups.  5-azac treatment leads to more sites with loss of M.   

```{r,echo=FALSE}
plotDensity(meth.mat.sans.NA_allsd.allM[,c(1:10),with=FALSE],ylim=c(0,0.8),main=paste0("NOT normalised. N=",nrow(meth.mat.sans.NA_allsd.allM)," sites.all M values.10 samples only shown"),col = ppcolors2,xlab="% Methylation")
legend("topright", legend=grps, text.col=pcolors,box.lty=0)
```
<br>
***Sample outliers with their discordant high methylation profile are cleary seen in the density plot below.  Peak at around 100%M:  outliers are the highest peak [solid red] and lowest peak [dashed reds].***

```{r,echo=FALSE}
plotDensity(meth.mat.sans.NA_highsd.allM,ylim=c(0,0.12),main=paste0("NOT normalised. most variable sites N=",nrow(meth.mat.sans.NA_highsd.allM),". all M values"),col = ppcolors2,xlab="% Methylation")
legend("topright", legend=grps, text.col=pcolors,box.lty=0)
```
<br>
<br>
**Summary statistics of the methylation values per sample.  Most variable sites only.**\
Note the higher Methylation profile of sample **T2-A1-B1** and lower profile of samples **T2-A1-B2, T2-A1-B4**\
when compared to the rest of 5-azac samples and other samples in general.\
```{r,echo=FALSE}
out<-apply((meth.mat.sans.NA_highsd.allM),2,summary)
out[,order(colnames(out))]

```
<br>
<br>

#
***
***
***
#

<br>
<br>

###############################################################################################################################################

# Checking variance dispersion at v low and v high methylation
We do not see any bias of high variance at extreme M values ie at low and high M. 

```{r,echo=FALSE}

rowave<-rowMeans(meth.mat.sans.NA_highsd.allM)
rowmed<-rowMedians(as.matrix(meth.mat.sans.NA_highsd.allM))
rowsd<-rowSds(meth.mat.sans.NA_highsd.allM)
rowcv<-rowsd/rowave
message("Summary statistics of Sample Standard Deviation at each sites")
summary(rowsd)

data.cv<-data.frame(ave=rowave,median=rowmed,sd=rowsd)

## plotting sd vs ave
# hex binned scatter plot
ggplot(data.cv,aes(ave,sd))+stat_bin_hex(colour="white", na.rm=TRUE,bins = 56) +
  scale_fill_gradientn(colours=c("blue","magenta"),name = "Frequency", na.value=NA)+ theme_bw()+theme(text = element_text(size = 20))+ggtitle(paste0("Most variable sites N = ",nrow(meth.mat.sans.NA_highsd.allM)))+labs(x="mean %M")
# smoothed scatterplot
buylrd = c("#313695", "#4575B4", "#74ADD1", "#ABD9E9", "#E0F3F8", "#FFFFBF",
           "#FEE090", "#FDAE61", "#F46D43", "#D73027", "#A50026") 
myColRamp = colorRampPalette(c(buylrd))
smoothScatter(x=rowave,y=rowsd,
              colramp=myColRamp,col="black",
              main=paste0("Most variable sites N =",nrow(meth.mat.sans.NA_highsd.allM)),
              xlab="mean %M",
              ylab="sd %M", nbin=512)
#
ggplot(data.cv,aes(median,sd))+stat_bin_hex(colour="white", na.rm=TRUE,bins = 56) +
  scale_fill_gradientn(colours=c("blue","magenta"),name = "Frequency", na.value=NA)+ theme_bw()+theme(text = element_text(size = 20))+ggtitle(paste0("Most variable sites N =",nrow(meth.mat.sans.NA_highsd.allM)))+labs(x="median %M")
# smoothed scatterplot
smoothScatter(x=rowmed,y=rowsd,
              colramp=myColRamp,col="black",
              main=paste0("Most variable sites N =",nrow(meth.mat.sans.NA_highsd.allM)),
              xlab="median %M",
              ylab="sd %M", nbin=512)


```
<br>
<br>

#
***
***
***
#

<br>
<br>

#############################################################################################################

# Variance decomposition of all samples in different treatments and F-generations

#
***
#

<br>
<br>

## NO normalisation - *_all_* M values


```{r,echo=FALSE}

# subset matrix to highly variable only  !!!

##  no normalisation  !!! 
dat<-meth.mat.sans.NA_highsd.allM
dd <- as.dist(1-cor(dat))

#mds <- cmdscale(dd, eig=TRUE,k=10)

pc <- prcomp(t(dat))
pc_var_explained <- (pc$sdev^2/sum(pc$sdev^2))[1:10]
pc_var_explained <- round((pc_var_explained*100), 2)
barplot(pc_var_explained, xlab="PC1 to PC10", ylab="Proportion of variance (%)",)


plot(pc$x[,1],pc$x[,2], pch=16, xlab=paste('Principal Component 1 ',pc_var_explained[1],"%"),ylab=paste('Principal Component 2 ',pc_var_explained[2],"%"),asp=1,col=ppcolors2)
legend("topright", legend=grps, text.col=pcolors,box.lty=0)
#legend("topright", inset=c(-0.2,0), legend=grps, text.col=pcolors,box.lty=0, title="Group")
plot(pc$x[,1],pc$x[,3], pch=16, xlab=paste('Principal Component 1 ',pc_var_explained[1],"%"),ylab=paste('Principal Component 3 ',pc_var_explained[3],"%"),asp=1,col=ppcolors2)
legend("topright", legend=grps, text.col=pcolors,box.lty=0)
plot(pc$x[,2],pc$x[,3], pch=16, xlab=paste('Principal Component 2 ',pc_var_explained[2],"%"),ylab=paste('Principal Component 3 ',pc_var_explained[3],"%"),asp=1,col=ppcolors2)
legend("topright", legend=grps, text.col=pcolors,box.lty=0)


# unsupervised clustering


dend<-as.dendrogram(hclust(dd, method = "ward.D2", members = NULL))
colors_to_use <-ppcolors2
colors_to_use <- colors_to_use[order.dendrogram(dend)]
labels_colors(dend) <- colors_to_use
dend<-set(dend, "labels_cex", 0.8)
plot(dend,cex=0.5,main=" ",ylab="Ward:   1 - correlation")

#`r knitr::knit_exit()`
```

<br>
<br>

#
***
#

<br>
<br>

## Zscore normalisation - *_all_* M values

```{r,echo=FALSE}

# subset matrix to highly variable only  !!!

##  zscore normalisation  !!! 
dat<-scale(meth.mat.sans.NA_highsd.allM,center = TRUE, scale = TRUE)
dd <- as.dist(1-cor(dat))

#mds <- cmdscale(dd, eig=TRUE,k=10)

pc <- prcomp(t(dat))
pc_var_explained <- (pc$sdev^2/sum(pc$sdev^2))[1:10]
pc_var_explained <- round((pc_var_explained*100), 2)
barplot(pc_var_explained, xlab="PC1 to PC10", ylab="Proportion of variance (%)",)


plot(pc$x[,1],pc$x[,2], pch=16, xlab=paste('Principal Component 1 ',pc_var_explained[1],"%"),ylab=paste('Principal Component 2 ',pc_var_explained[2],"%"),asp=1,col=ppcolors2)
legend("topright", legend=grps, text.col=pcolors,box.lty=0)
#legend("topright", inset=c(-0.2,0), legend=grps, text.col=pcolors,box.lty=0, title="Group")
plot(pc$x[,1],pc$x[,3], pch=16, xlab=paste('Principal Component 1 ',pc_var_explained[1],"%"),ylab=paste('Principal Component 3 ',pc_var_explained[3],"%"),asp=1,col=ppcolors2)
legend("topright", legend=grps, text.col=pcolors,box.lty=0)
plot(pc$x[,2],pc$x[,3], pch=16, xlab=paste('Principal Component 2 ',pc_var_explained[2],"%"),ylab=paste('Principal Component 3 ',pc_var_explained[3],"%"),asp=1,col=ppcolors2)
legend("topright", legend=grps, text.col=pcolors,box.lty=0)

# unsupervised clustering

dend<-as.dendrogram(hclust(dd, method = "ward.D2", members = NULL))
colors_to_use <-ppcolors2
colors_to_use <- colors_to_use[order.dendrogram(dend)]
labels_colors(dend) <- colors_to_use
dend<-set(dend, "labels_cex", 0.8)
plot(dend,cex=0.5,main=" ",ylab="Ward:   1 - correlation")


```


<br>
<br>

#
***
#

<br>
<br>


## Quantile normalisation - *_all_* M values

```{r,echo=FALSE}

# subset matrix to highly variable only !!!


## quantile normalise 
dat<-preprocessCore::normalize.quantiles(as.matrix(meth.mat.sans.NA_highsd.allM),copy = FALSE)
dd <- as.dist(1-cor(dat))

#mds <- cmdscale(dd, eig=TRUE,k=10)

pc <- prcomp(t(dat))
pc_var_explained <- (pc$sdev^2/sum(pc$sdev^2))[1:10]
pc_var_explained <- round((pc_var_explained*100), 2)
  
barplot(pc_var_explained, xlab="PC1 to PC10", ylab="Proportion of variance (%)",)


plot(pc$x[,1],pc$x[,2], pch=16, xlab=paste('Principal Component 1 ',pc_var_explained[1],"%"),ylab=paste('Principal Component 2 ',pc_var_explained[2],"%"),asp=1,col=ppcolors2)
legend("topright", legend=grps, text.col=pcolors,box.lty=0)
plot(pc$x[,1],pc$x[,3], pch=16, xlab=paste('Principal Component 1 ',pc_var_explained[1],"%"),ylab=paste('Principal Component 3 ',pc_var_explained[3],"%"),asp=1,col=ppcolors2)
legend("topright", legend=grps, text.col=pcolors,box.lty=0)
plot(pc$x[,2],pc$x[,3], pch=16, xlab=paste('Principal Component 2 ',pc_var_explained[2],"%"),ylab=paste('Principal Component 3 ',pc_var_explained[3],"%"),asp=1,col=ppcolors2)
legend("topright", legend=grps, text.col=pcolors,box.lty=0)

# unsupervised clustering

dend<-as.dendrogram(hclust(dd, method = "ward.D2", members = NULL))
colors_to_use <-ppcolors2
colors_to_use <- colors_to_use[order.dendrogram(dend)]
labels_colors(dend) <- colors_to_use
dend<-set(dend, "labels_cex", 0.8)
plot(dend,cex=0.5,main=" ",ylab="Ward:   1 - correlation")



```


<br>
<br>

#
***
#

<br>
<br>



## NO normalisation - *_low M_* values only, median M <= 20% across samples  

```{r,echo=FALSE}

# subset matrix to highly variable only  !!!

##  no normalisation  !!! 
dat<-meth.mat.sans.NA
dd <- as.dist(1-cor(dat))

#mds <- cmdscale(dd, eig=TRUE,k=10)

pc <- prcomp(t(dat))
pc_var_explained <- (pc$sdev^2/sum(pc$sdev^2))[1:10]
pc_var_explained <- round((pc_var_explained*100), 2)
barplot(pc_var_explained, xlab="PC1 to PC10", ylab="Proportion of variance (%)",)


plot(pc$x[,1],pc$x[,2], pch=16, xlab=paste('Principal Component 1 ',pc_var_explained[1],"%"),ylab=paste('Principal Component 2 ',pc_var_explained[2],"%"),asp=1,col=ppcolors2)
legend("topright", legend=grps, text.col=pcolors,box.lty=0)
#legend("topright", inset=c(-0.2,0), legend=grps, text.col=pcolors,box.lty=0, title="Group")
plot(pc$x[,1],pc$x[,3], pch=16, xlab=paste('Principal Component 1 ',pc_var_explained[1],"%"),ylab=paste('Principal Component 3 ',pc_var_explained[3],"%"),asp=1,col=ppcolors2)
legend("topright", legend=grps, text.col=pcolors,box.lty=0)
plot(pc$x[,2],pc$x[,3], pch=16, xlab=paste('Principal Component 2 ',pc_var_explained[2],"%"),ylab=paste('Principal Component 3 ',pc_var_explained[3],"%"),asp=1,col=ppcolors2)
legend("topright", legend=grps, text.col=pcolors,box.lty=0)

# unsupervised clustering

dend<-as.dendrogram(hclust(dd, method = "ward.D2", members = NULL))
colors_to_use <-ppcolors2
colors_to_use <- colors_to_use[order.dendrogram(dend)]
labels_colors(dend) <- colors_to_use
dend<-set(dend, "labels_cex", 0.8)
plot(dend,cex=0.5,main=" ",ylab="Ward:   1 - correlation")


```

<br>
<br>

#
***
#

<br>
<br>

## Zscore normalisation - *_low M_* values only , median M <= 20% across samples 

```{r,echo=FALSE}

# subset matrix to highly variable only  !!!

##  Z- score  !!! 
dat<-scale(meth.mat.sans.NA,center = TRUE, scale = TRUE)
dd <- as.dist(1-cor(dat))

#mds <- cmdscale(dd, eig=TRUE,k=10)

pc <- prcomp(t(dat))
pc_var_explained <- (pc$sdev^2/sum(pc$sdev^2))[1:10]
pc_var_explained <- round((pc_var_explained*100), 2)
barplot(pc_var_explained, xlab="PC1 to PC10", ylab="Proportion of variance (%)",)


plot(pc$x[,1],pc$x[,2], pch=16, xlab=paste('Principal Component 1 ',pc_var_explained[1],"%"),ylab=paste('Principal Component 2 ',pc_var_explained[2],"%"),asp=1,col=ppcolors2)
legend("topright", legend=grps, text.col=pcolors,box.lty=0)
#legend("topright", inset=c(-0.2,0), legend=grps, text.col=pcolors,box.lty=0, title="Group")
plot(pc$x[,1],pc$x[,3], pch=16, xlab=paste('Principal Component 1 ',pc_var_explained[1],"%"),ylab=paste('Principal Component 3 ',pc_var_explained[3],"%"),asp=1,col=ppcolors2)
legend("topright", legend=grps, text.col=pcolors,box.lty=0)
plot(pc$x[,2],pc$x[,3], pch=16, xlab=paste('Principal Component 2 ',pc_var_explained[2],"%"),ylab=paste('Principal Component 3 ',pc_var_explained[3],"%"),asp=1,col=ppcolors2)
legend("topright", legend=grps, text.col=pcolors,box.lty=0)

# unsupervised clustering

dend<-as.dendrogram(hclust(dd, method = "ward.D2", members = NULL))
colors_to_use <-ppcolors2
colors_to_use <- colors_to_use[order.dendrogram(dend)]
labels_colors(dend) <- colors_to_use
dend<-set(dend, "labels_cex", 0.8)
plot(dend,cex=0.5,main=" ",ylab="Ward:   1 - correlation")


```

<br>
<br>

#
***
#

<br>
<br>
  
## Quantile normalisation - *_low M_* values only , median M <= 20% across samples 

```{r,echo=FALSE}

# subset matrix to highly variable only !!!


## quantile normalise 
dat<-preprocessCore::normalize.quantiles(as.matrix(meth.mat.sans.NA),copy = FALSE)
dd <- as.dist(1-cor(dat))

#mds <- cmdscale(dd, eig=TRUE,k=10)

pc <- prcomp(t(dat))
pc_var_explained <- (pc$sdev^2/sum(pc$sdev^2))[1:10]
pc_var_explained <- round((pc_var_explained*100), 2)
  
barplot(pc_var_explained, xlab="PC1 to PC10", ylab="Proportion of variance (%)",)


plot(pc$x[,1],pc$x[,2], pch=16, xlab=paste('Principal Component 1 ',pc_var_explained[1],"%"),ylab=paste('Principal Component 2 ',pc_var_explained[2],"%"),asp=1,col=ppcolors2)
legend("topright", legend=grps, text.col=pcolors,box.lty=0)
plot(pc$x[,1],pc$x[,3], pch=16, xlab=paste('Principal Component 1 ',pc_var_explained[1],"%"),ylab=paste('Principal Component 3 ',pc_var_explained[3],"%"),asp=1,col=ppcolors2)
legend("topright", legend=grps, text.col=pcolors,box.lty=0)
plot(pc$x[,2],pc$x[,3], pch=16, xlab=paste('Principal Component 2 ',pc_var_explained[2],"%"),ylab=paste('Principal Component 3 ',pc_var_explained[3],"%"),asp=1,col=ppcolors2)
legend("topright", legend=grps, text.col=pcolors,box.lty=0)

# unsupervised clustering

dend<-as.dendrogram(hclust(dd, method = "ward.D2", members = NULL))
colors_to_use <-ppcolors2
colors_to_use <- colors_to_use[order.dendrogram(dend)]
labels_colors(dend) <- colors_to_use
dend<-set(dend, "labels_cex", 0.8)
plot(dend,cex=0.5,main=" ",ylab="Ward:   1 - correlation")



```
<br>
<br>

#
***
#

<br>
<br>




## NO normalisation - *_high M_* values only, median M >= 80% across samples 

```{r,echo=FALSE}

# subset matrix to highly variable only  !!!

##  no normalisation  !!! 
dat<-meth.mat.sans.NA_highsd.highM
dd <- as.dist(1-cor(dat))

#mds <- cmdscale(dd, eig=TRUE,k=10)

pc <- prcomp(t(dat))
pc_var_explained <- (pc$sdev^2/sum(pc$sdev^2))[1:10]
pc_var_explained <- round((pc_var_explained*100), 2)
barplot(pc_var_explained, xlab="PC1 to PC10", ylab="Proportion of variance (%)",)


plot(pc$x[,1],pc$x[,2], pch=16, xlab=paste('Principal Component 1 ',pc_var_explained[1],"%"),ylab=paste('Principal Component 2 ',pc_var_explained[2],"%"),asp=1,col=ppcolors2)
legend("topright", legend=grps, text.col=pcolors,box.lty=0)
#legend("topright", inset=c(-0.2,0), legend=grps, text.col=pcolors,box.lty=0, title="Group")
plot(pc$x[,1],pc$x[,3], pch=16, xlab=paste('Principal Component 1 ',pc_var_explained[1],"%"),ylab=paste('Principal Component 3 ',pc_var_explained[3],"%"),asp=1,col=ppcolors2)
legend("topright", legend=grps, text.col=pcolors,box.lty=0)
plot(pc$x[,2],pc$x[,3], pch=16, xlab=paste('Principal Component 2 ',pc_var_explained[2],"%"),ylab=paste('Principal Component 3 ',pc_var_explained[3],"%"),asp=1,col=ppcolors2)
legend("topright", legend=grps, text.col=pcolors,box.lty=0)

# unsupervised clustering

dend<-as.dendrogram(hclust(dd, method = "ward.D2", members = NULL))
colors_to_use <-ppcolors2
colors_to_use <- colors_to_use[order.dendrogram(dend)]
labels_colors(dend) <- colors_to_use
dend<-set(dend, "labels_cex", 0.8)
plot(dend,cex=0.5,main=" ",ylab="Ward:   1 - correlation")


```

<br>
<br>

#
***
#

<br>
<br>

## Zscore normalisation - *_high M_* values only, median M >= 80% across samples 

```{r,echo=FALSE}

# subset matrix to highly variable only  !!!

##  zscore normalisation  !!! 
dat<-scale(meth.mat.sans.NA_highsd.highM,center = TRUE, scale = TRUE)
dd <- as.dist(1-cor(dat))

#mds <- cmdscale(dd, eig=TRUE,k=10)

pc <- prcomp(t(dat))
pc_var_explained <- (pc$sdev^2/sum(pc$sdev^2))[1:10]
pc_var_explained <- round((pc_var_explained*100), 2)
barplot(pc_var_explained, xlab="PC1 to PC10", ylab="Proportion of variance (%)",)


plot(pc$x[,1],pc$x[,2], pch=16, xlab=paste('Principal Component 1 ',pc_var_explained[1],"%"),ylab=paste('Principal Component 2 ',pc_var_explained[2],"%"),asp=1,col=ppcolors2)
legend("topright", legend=grps, text.col=pcolors,box.lty=0)
#legend("topright", inset=c(-0.2,0), legend=grps, text.col=pcolors,box.lty=0, title="Group")
plot(pc$x[,1],pc$x[,3], pch=16, xlab=paste('Principal Component 1 ',pc_var_explained[1],"%"),ylab=paste('Principal Component 3 ',pc_var_explained[3],"%"),asp=1,col=ppcolors2)
legend("topright", legend=grps, text.col=pcolors,box.lty=0)
plot(pc$x[,2],pc$x[,3], pch=16, xlab=paste('Principal Component 2 ',pc_var_explained[2],"%"),ylab=paste('Principal Component 3 ',pc_var_explained[3],"%"),asp=1,col=ppcolors2)
legend("topright", legend=grps, text.col=pcolors,box.lty=0)

# unsupervised clustering

dend<-as.dendrogram(hclust(dd, method = "ward.D2", members = NULL))
colors_to_use <-ppcolors2
colors_to_use <- colors_to_use[order.dendrogram(dend)]
labels_colors(dend) <- colors_to_use
dend<-set(dend, "labels_cex", 0.8)
plot(dend,cex=0.5,main=" ",ylab="Ward:   1 - correlation")


```

<br>
<br>

#
***
#

<br>
<br>

## Quantile normalisation - *_high M_* values only, median M >= 80% across samples 

```{r,echo=FALSE}

# subset matrix to highly variable only !!!


## quantile normalise 
dat<-preprocessCore::normalize.quantiles(as.matrix(meth.mat.sans.NA_highsd.highM),copy = FALSE)
dd <- as.dist(1-cor(dat))

#mds <- cmdscale(dd, eig=TRUE,k=10)

pc <- prcomp(t(dat))
pc_var_explained <- (pc$sdev^2/sum(pc$sdev^2))[1:10]
pc_var_explained <- round((pc_var_explained*100), 2)
  
barplot(pc_var_explained, xlab="PC1 to PC10", ylab="Proportion of variance (%)",)


plot(pc$x[,1],pc$x[,2], pch=16, xlab=paste('Principal Component 1 ',pc_var_explained[1],"%"),ylab=paste('Principal Component 2 ',pc_var_explained[2],"%"),asp=1,col=ppcolors2)
legend("topright", legend=grps, text.col=pcolors,box.lty=0)
plot(pc$x[,1],pc$x[,3], pch=16, xlab=paste('Principal Component 1 ',pc_var_explained[1],"%"),ylab=paste('Principal Component 3 ',pc_var_explained[3],"%"),asp=1,col=ppcolors2)
legend("topright", legend=grps, text.col=pcolors,box.lty=0)
plot(pc$x[,2],pc$x[,3], pch=16, xlab=paste('Principal Component 2 ',pc_var_explained[2],"%"),ylab=paste('Principal Component 3 ',pc_var_explained[3],"%"),asp=1,col=ppcolors2)
legend("topright", legend=grps, text.col=pcolors,box.lty=0)

# unsupervised clustering

dend<-as.dendrogram(hclust(dd, method = "ward.D2", members = NULL))
colors_to_use <-ppcolors2
colors_to_use <- colors_to_use[order.dendrogram(dend)]
labels_colors(dend) <- colors_to_use
dend<-set(dend, "labels_cex", 0.8)
plot(dend,cex=0.5,main=" ",ylab="Ward:   1 - correlation")



```
<br>
<br>

#
***
***
***
#

<br>
<br>


# Probing possible sample outliers
Differenr colour scale is used here.  Please refer to figure legend.
<br>
<br>
Note the higher Methylation profile of sample **T2-A1-B1** and lower profile of samples **T2-A1-B2, T2-A1-B4**\
when compared to the rest of 5-azac samples (green).\
<br>
<br>

Mean is red filled circle.  Median is red bar.\ 
```{r fig.width=12, fig.height=8,echo=FALSE, warning=FALSE,fig.fullwidth=TRUE}

data.meth<-melt(meth.mat.sans.NA_allsd.highM[,order(colnames(meth.mat.sans.NA_allsd.highM)),with=FALSE])
ggplot(data.meth,aes(variable,value))+geom_violin(trim=FALSE,aes(fill=factor(substr(variable,1,4))))+stat_summary(fun.data=mean_sdl, geom="pointrange", color="red")+ stat_summary(fun.y = median, fun.ymin = median, fun.ymax = median,geom = "crossbar", width = 0.5, color="red")+ylim(80,100)+theme_bw()+theme(axis.text.x = element_text(angle = 90))+ggtitle(paste0("high M only. N = ",nrow(meth.mat.sans.NA_allsd.highM)))+theme(legend.title = element_blank())


data.meth<-melt(meth.mat.sans.NA_highsd.highM[,order(colnames(meth.mat.sans.NA_highsd.highM)),with=FALSE])
ggplot(data.meth,aes(variable,value))+geom_violin(trim=FALSE,aes(fill=factor(substr(variable,1,4))))+stat_summary(fun.data=mean_sdl, geom="pointrange", color="red")+stat_summary(fun.y = median, fun.ymin = median, fun.ymax = median,geom = "crossbar", width = 0.5, color="red")+ylim(80,100)+theme_bw()+theme(axis.text.x = element_text(angle = 90))+ggtitle(paste0("most variable high M only. N = ",nrow(meth.mat.sans.NA_highsd.highM)))+theme(legend.title = element_blank())

reduced<-meth.mat.sans.NA[rowMeans(as.matrix(meth.mat.sans.NA))<=2.5,]
data.meth<-melt(reduced[,order(colnames(reduced)),with=FALSE])
ggplot(data.meth,aes(variable,value))+geom_violin(trim=FALSE,aes(fill=factor(substr(variable,1,4))))+stat_summary(fun.data=mean_sdl, geom="pointrange", color="red")+stat_summary(fun.y = median, fun.ymin = median, fun.ymax = median,geom = "crossbar", width = 0.5, color="red")+ylim(0,0.1)+theme_bw()+theme(axis.text.x = element_text(angle = 90))+ggtitle(paste0("most variable low M only. N = ",nrow(reduced)))+theme(legend.title = element_blank())


```





<!--

###############################################################################################################################################
## Sample Outlier


```{r,echo=FALSE}
plotDensity(meth.mat.sans.NA,ylim=c(0,0.55),main="NOT normalised ",col = ppcolors2)
```
<br>
```{r,echo=FALSE}
# remove outliers
outlier<-"T2-C4-B4"
message("Removing sample outliers")
outlier
#meth.mat.sans.NA<-meth.mat.sans.NA[,-which(colnames(meth.mat.sans.NA)==outlier),with=FALSE]
```
<br>
```{r,echo=FALSE}
plotDensity(meth.mat.sans.NA,ylim=c(0,0.55),main="NOT normalised - minus outlier",col = ppcolors2)
```
```{r,echo=FALSE}
plotDensity(meth.mat.sans.NA,ylim=c(0,0.55),xlim=c(-4,15),main="NOT normalised - minus outlier -- zoom in at lower %M",col = ppcolors2)

```



#############################################################################################################
## checking density of Methylation values



```{r,echo=FALSE}
plotDensity(scale(meth.mat.sans.NA),main="zscore normalised")
plotDensity(scale(meth.mat.sans.NA),xlim=c(-1,15),main="zscore normalised -- zoom in at lower %M")
```

```{r,echo=FALSE}
plotDensity(preprocessCore::normalize.quantiles(as.matrix(meth.mat.sans.NA)),ylim=c(0,0.45),main="quantile normalised")
```

```{r,echo=FALSE}
plotDensity(preprocessCore::normalize.quantiles(as.matrix(meth.mat.sans.NA)),ylim=c(0,0.45),xlim=c(-4,15),main="quantile normalised -- zoom in at lower %M")
```
<br>
<br>
Summary statistics of the methylation values per sample.
```{r,echo=FALSE}

#boxplot -- not useful
data.meth<-melt(meth.mat.sans.NA_highsd.allM)
ggplot(data.meth,aes(variable,value))+geom_violin(trim=FALSE)+stat_summary(fun.data=mean_sdl, geom="pointrange", color="red")
out<-apply((meth.mat.sans.NA),2,summary)
out[,order(colnames(out))]
```
<br>
<br>
95% percentile of the methylation values per sample
```{r,echo=FALSE}
#get 95% percentile
getquant<-function(x){quantile(x,0.95)}
as.numeric(apply((meth.mat.sans.NA),2,getquant ))



###############################################################################################################################################
## checking variance dispersion at v low and v high methylation





```{r,echo=FALSE}

rowave<-rowMeans(meth.mat.sans.NA)
rowsd<-rowSds(meth.mat.sans.NA)
rowcv<-rowsd/rowave
message("Summary statistics of Mean")
summary(rowave)
message("Summary statistics of Standard Deviation")
summary(rowsd)
message("Summary statistics of CV")
summary(rowcv)

data.cv<-data.frame(ave=rowave,sd=rowsd,cv=rowcv)
```
<br>
<br>

CV Coefficient of Variation is higher at low methylation values.  As expected, as CV is divided by the mean. 
```{r,echo=FALSE}
## plotting cv vs ave
#hex binned scatter plot
ggplot(data.cv,aes(ave,cv))+stat_bin_hex(colour="white", na.rm=TRUE,bins = 56) +
  scale_fill_gradientn(colours=c("blue","magenta"),name = "Frequency", na.value=NA)+ theme_bw()+theme(text = element_text(size = 20))+ggtitle("")
  


# smoothed scatterplot
buylrd = c("#313695", "#4575B4", "#74ADD1", "#ABD9E9", "#E0F3F8", "#FFFFBF",
           "#FEE090", "#FDAE61", "#F46D43", "#D73027", "#A50026") 
myColRamp = colorRampPalette(c(buylrd))
smoothScatter(x=rowave,y=rowcv,
              colramp=myColRamp,col="black",
              main="",
              xlab="mean %M",
              ylab="cv %M", nbin=512)

```
<br>
<br>

Standard deviation is low at the extreme values of 0%,100% M and increases at towards the median methylation.



-->
