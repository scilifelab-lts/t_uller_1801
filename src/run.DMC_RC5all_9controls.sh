#!/bin/bash -l


#fullmeta=$1
fullmeta="/proj/snic2020-16-42/louella/Bismark.04242020.FINAL/metadata.uppmax.FINAL_10302020.tsv"


DMC="/proj/snic2020-16-42/louella/src/DMC.R"



batch="T2"
control="C"
#treats=(A)
#gens=(1)

treats=(A L T)
gens=(1 2 4)


for treat in ${treats[@]}
do
	for gen in ${gens[@]}
	do
	#echo "$batch $treat  $gen"
	echo -e "/${batch}-${treat}${gen}-"

	ometa="${batch}-${treat}${gen}.metadata.tsv"

	head -n1 $fullmeta > $ometa
	key=$(echo -e "/${batch}-${treat}${gen}-")
	cat $fullmeta | awk -F"\t" -v ID="$key" '$1~ID'  >> $ometa
	key=$(echo -e "/${batch}-${control}")
        cat $fullmeta | awk -F"\t" -v ID="$key" '$1~ID'  >> $ometa

	#sbatch -A snic2020-15-61 -p core -n 2 -t 120:00:00 -J DMC -e DMC_%j_error.txt -o DMC_%j_out.txt Rscript $DMC -m $ometa -t 2 -q 0.2


	osh="${batch}-${treat}${gen}.metadata.sh"
	ojob="${batch}-${treat}${gen}"
	
        cat > $osh << EOF
#!/bin/bash -l

#SBATCH -A snic2020-15-61
#SBATCH -p core
#SBATCH -n 8
#SBATCH -t 48:00:00
#SBATCH -J $ojob
#SBATCH -e ${ojob}_%j_error.txt
#SBATCH -o ${ojob}_%j_out.txt

module load bioinfo-tools

Rscript $DMC -m $ometa -t 8 -q 0.05 -c 5 -s 0

exit 0;
EOF






	sbatch $osh
	echo
	done
done
#exit 1


batch="T1"
control="C"
treats=(Z)
gens=(1 2 4)


for treat in ${treats[@]}
do
        for gen in ${gens[@]}
        do
        #echo "$batch $treat  $gen"
        echo -e "/${batch}-${treat}${gen}-"

        ometa="${batch}-${treat}${gen}.metadata.tsv"

        head -n1 $fullmeta > $ometa
        key=$(echo -e "/${batch}-${treat}${gen}-")
        cat $fullmeta | awk -F"\t" -v ID="$key" '$1~ID'  >> $ometa
        key=$(echo -e "/${batch}-${control}")
        cat $fullmeta | awk -F"\t" -v ID="$key" '$1~ID'  >> $ometa


        osh="${batch}-${treat}${gen}.metadata.sh"
        ojob="${batch}-${treat}${gen}"

        cat > $osh << EOF
#!/bin/bash -l

#SBATCH -A snic2020-15-61
#SBATCH -p core
#SBATCH -n 8
#SBATCH -t 48:00:00
#SBATCH -J $ojob
#SBATCH -e ${ojob}_%j_error.txt
#SBATCH -o ${ojob}_%j_out.txt

module load bioinfo-tools

Rscript $DMC -m $ometa -t 8 -q 0.05 -c 5 -s 0

exit 0;
EOF






        sbatch $osh



        echo
        done
done


## All generations of T1-Z

head -n1 T1-Z1.metadata.tsv > T1-Z0.metadata.tsv
cat T1-Z*.metadata.tsv | grep -v file | sort | uniq | sort -nk3,3 |  awk -F"\t" '{gsub(/T1-C/,"T1-C0",$2);gsub(/T1-Z/,"T1-Z0",$2);print}' OFS="\t" >> T1-Z0.metadata.tsv


osh="T1-Z0.metadata.sh"
ojob="T1-Z0"

cat > $osh << EOF
#!/bin/bash -l

#SBATCH -A snic2020-15-61
#SBATCH -p core
#SBATCH -n 8
#SBATCH -t 48:00:00
#SBATCH -J $ojob
#SBATCH -e ${ojob}_%j_error.txt
#SBATCH -o ${ojob}_%j_out.txt

module load bioinfo-tools

Rscript $DMC -m T1-Z0.metadata.tsv -t 8 -q 0.05 -c 5

exit 0;
EOF

sbatch $osh



## All generations of T2-A

head -n1 T2-A1.metadata.tsv > T2-A0.metadata.tsv
cat T2-A*.metadata.tsv | grep -v file | sort | uniq | sort -nk3,3 |  awk -F"\t" '{gsub(/T2-C/,"T2-C0",$2);gsub(/T2-A/,"T2-A0",$2);print}' OFS="\t" >> T2-A0.metadata.tsv


osh="T2-A0.metadata.sh"
ojob="T2-A0"

cat > $osh << EOF
#!/bin/bash -l

#SBATCH -A snic2020-15-61
#SBATCH -p core
#SBATCH -n 8
#SBATCH -t 48:00:00
#SBATCH -J $ojob
#SBATCH -e ${ojob}_%j_error.txt
#SBATCH -o ${ojob}_%j_out.txt

module load bioinfo-tools

Rscript $DMC -m T2-A0.metadata.tsv -t 8 -q 0.05 -c 5

exit 0;
EOF

sbatch $osh



## All generations of T2-L

head -n1 T2-L1.metadata.tsv > T2-L0.metadata.tsv
cat T2-L*.metadata.tsv | grep -v file | sort | uniq | sort -nk3,3 |  awk -F"\t" '{gsub(/T2-C/,"T2-C0",$2);gsub(/T2-L/,"T2-L0",$2);print}' OFS="\t" >> T2-L0.metadata.tsv


osh="T2-L0.metadata.sh"
ojob="T2-L0"

cat > $osh << EOF
#!/bin/bash -l

#SBATCH -A snic2020-15-61
#SBATCH -p core
#SBATCH -n 8
#SBATCH -t 48:00:00
#SBATCH -J $ojob
#SBATCH -e ${ojob}_%j_error.txt
#SBATCH -o ${ojob}_%j_out.txt

module load bioinfo-tools

Rscript $DMC -m T2-L0.metadata.tsv -t 8 -q 0.05 -c 5

exit 0;
EOF

sbatch $osh



## All generations of T2-T

head -n1 T2-T1.metadata.tsv > T2-T0.metadata.tsv
cat T2-T*.metadata.tsv | grep -v file | sort | uniq | sort -nk3,3 |  awk -F"\t" '{gsub(/T2-C/,"T2-C0",$2);gsub(/T2-T/,"T2-T0",$2);print}' OFS="\t" >> T2-T0.metadata.tsv


osh="T2-T0.metadata.sh"
ojob="T2-T0"

cat > $osh << EOF
#!/bin/bash -l

#SBATCH -A snic2020-15-61
#SBATCH -p core
#SBATCH -n 8
#SBATCH -t 48:00:00
#SBATCH -J $ojob
#SBATCH -e ${ojob}_%j_error.txt
#SBATCH -o ${ojob}_%j_out.txt

module load bioinfo-tools

Rscript $DMC -m T2-T0.metadata.tsv -t 8 -q 0.05 -c 5

exit 0;
EOF

sbatch $osh

