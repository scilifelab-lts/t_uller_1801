#!/bin/bash -l

#SBATCH -A snic2020-15-61
#SBATCH -p core
#SBATCH -n 8
#SBATCH -t 120:00:00
#SBATCH -J sd.union 
#SBATCH -e sd.union_%j_error.txt
#SBATCH -o sd.union_%j_out.txt

module load bioinfo-tools
module load BEDTools/2.27.1
module load BEDOPS
module load gnuparallel


######################
# Note !!!
# NEED to SORT bedgraph first before using bedtools unionbedg


indir="/proj/snic2020-16-42/louella/Bismark.04242020.FINAL/bismark_methylation_calls/bedGraph"
parallel "name=\$(echo {} | awk -F/ '{print \$NF}' | sed 's/.gz//');zcat {} | sort-bed - >  ${indir}/sorted.\$name; echo sorted.\$name" ::: $(ls ${indir}/*_bismark_bt2_pe.deduplicated.bedGraph.gz)

echo "DONE sorting bedGraphs"


######################
# consider all 64 coverage files

infiles=$(ls ${indir}/sorted.*_bismark_bt2_pe.deduplicated.bedGraph | sort -nk1,1 | tr "\n" " " | xargs)
innames=$(ls ${indir}/sorted.*_bismark_bt2_pe.deduplicated.bedGraph | sort -nk1,1 | awk -F/ '{print $NF}' | cut -d_ -f1 | cut -d. -f2- | tr "\n" " " | xargs)


bedtools unionbedg -i $infiles -header -names $innames -filler NA  > ${indir}/coverage_union.txt

echo "DONE unionbed all bedgraph"

######################
# remove failed samples due to unique mapping rate <15%

remove="${indir}/list.samples.failed_mapping_rate.txt"
infiles=$(ls ${indir}/sorted.*_bismark_bt2_pe.deduplicated.bedGraph | grep -v -f $remove | sort -nk1,1 | tr "\n" " " | xargs)
innames=$(ls ${indir}/sorted.*_bismark_bt2_pe.deduplicated.bedGraph | grep -v -f $remove | sort -nk1,1 | awk -F/ '{print $NF}' | cut -d_ -f1 | cut -d. -f2- | tr "\n" " " | xargs)


bedtools unionbedg -i $infiles -header -names $innames -filler NA  > ${indir}/coverage_union_mapping15.txt

echo "DONE unionbed 58 bedgraph"

######################
# remove sites where NA in at least one sample
echo -n > ${indir}/coverage_union_mapping15.NAremoved.txt
parallel --keep-order "echo {} | grep -v NA >> ${indir}/coverage_union_mapping15.NAremoved.txt" :::: ${indir}/coverage_union_mapping15.txt

echo "DONE remove NAs from coverage union"

######################
# compute for row sd 
# using matrix ${indir}/coverage_union_mapping15.NAremoved.txt

echo "getting row sd from coverage_union_mapping15.NAremoved.txt"
echo -n > ${indir}/sd_coverage_union_mapping15.NAremoved.txt

wc -l ${indir}/coverage_union_mapping15.NAremoved.txt

cut -f4- ${indir}/coverage_union_mapping15.NAremoved.txt | sed 1d > ${indir}/tmp.coverage_union_mapping15.NAremoved.txt
wc -l ${indir}/tmp.coverage_union_mapping15.NAremoved.txt

parallel --keep-order "inrow=$(echo {}); Rscript -e 'args <- commandArgs(TRUE); sd(args)' \$inrow | cut -d' ' -f2" :::: ${indir}/tmp.coverage_union_mapping15.NAremoved.txt >> ${indir}/sd_coverage_union_mapping15.NAremoved.txt
#head -n 20 ${indir}/coverage_union_mapping15.NAremoved.txt | cut -f4- | sed 1d | parallel --keep-order "inrow=$(echo {}); Rscript -e 'args <- commandArgs(TRUE); sd(args)' \$inrow | cut -d' ' -f2" >> ${indir}/sd_coverage_union_mapping15.NAremoved.txt
